if (process.env.NODE_ENV === 'production') {
  module.exports = {
    mongoURI: 'mongodb://omdjin:omdjin@ds257589.mlab.com:57589/vidjot-stg'
  };
} else {
  module.exports = { mongoURI: 'mongodb://localhost/vidjot-dev' };
}
