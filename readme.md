## About

This is a Node/Express app for jotting down ideas for future Youtube videos.

Version: 1.0.0

## Get Started

1. **Clone this project**. `git clone https://gitlab.com/omdjin/vidjot.git`

2. **Install dependencies**. `npm install`

3. **Start the project** `npm start`

### Live Demo

[Click Here!](https://guarded-stream-87353.herokuapp.com/)
